package ru.bigrussiandevelopers.vkonlinecheck;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;


public class VkWallParser {
    public static void main(String args[]){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd-HH:mm:ss");

        String logFileName = dateFormat.format(date) + ".log";
        Logger logger = Logger.getLogger("VkWallParser logger");
        FileHandler fh;
        try {
            fh = new FileHandler(logFileName);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info("Logger checking");
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        VkPageDownloader downloader = new VkPageDownloader("1156d0eb795139738a4963fa44d20bdfece9c70b979798a1fcbc1");
        VkWallParser parser = new VkWallParser(downloader, logger);
        try {
            parser.checkUserForAndroidPosts("595239");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public VkWallParser(VkPageDownloader downloader, Logger logger){
        this.downloader = downloader;
        this.logger = logger;

        httpProperties = new HashMap<String, String>();
        httpProperties.put(
                "Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
        );
    }

    public boolean checkUserForAndroidPosts(String userId) throws IOException {

        ArrayList<String> wallEntries = new ArrayList<String>();

        String vkontakteUserWallUrl = VKONTAKTE_URL_HTTPS +
                "wall" +
                userId +
                /* posts only by current user*/
                "?own=1";
        String htmlString;

        htmlString = downloader.getUrlAsString(vkontakteUserWallUrl);
        String wallItemPatternString = "<div id=\"post([0-9]+_[0-9]+).*?div class=\"replies_wrap";

        Pattern patternWallItem = Pattern.compile(wallItemPatternString);
        Matcher matcherWallItem = patternWallItem.matcher(htmlString);
        ArrayList<Pattern> patternsAndroid = new ArrayList<Pattern>();
        ArrayList<Matcher> matchersAndroid = new ArrayList<Matcher>();
        for(String appId : APP_IDS){
            String patternAndroidString = "app" + appId;
            Pattern patternAndroid = Pattern.compile(patternAndroidString);
            patternsAndroid.add(patternAndroid);
        }

        while(matcherWallItem.find()) {
            String wallEntry = matcherWallItem.group();
            if (wallEntry.indexOf("post_copy") == -1) {
                wallEntries.add(matcherWallItem.group());
            }
        }

        for (String wallEntry : wallEntries) {
            for (Pattern patternAndroid : patternsAndroid) {
                matchersAndroid.add(patternAndroid.matcher(wallEntry));
            }
            for (Matcher matcher : matchersAndroid) {
                while (matcher.find()) {
                    logger.info("Android detected, user: http://vk.com/id" + userId);
                    return true;
                }
            }
            matchersAndroid.clear();
        }
        return false;
    }

    private VkPageDownloader downloader;
    private Logger logger;
    private HashMap<String,String> httpProperties;

    private final String VKONTAKTE_URL_HTTPS = "https://vk.com/";
    private static ArrayList<String> APP_IDS;

    static {
        APP_IDS = new ArrayList<String>();
        APP_IDS.add("2274003");
        APP_IDS.add("2685278");
        APP_IDS.add("1921008");
    }
}
