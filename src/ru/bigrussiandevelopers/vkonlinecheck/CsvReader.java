package ru.bigrussiandevelopers.vkonlinecheck;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class CsvReader {

    private FileInputStream inputStream = null;
    private Scanner sc = null;

    public CsvReader(String filePath) throws IOException {

            inputStream = new FileInputStream(filePath);
            sc = new Scanner(inputStream, "UTF-8");
    }

    public String getNextId() {

        if (sc.hasNextLine()) {
            return sc.nextLine();
        } else {
            return null;
        }

        // note that Scanner suppresses exceptions
        //if (sc.ioException() != null) {
        //    throw sc.ioException();
        //}
    }

    public void close() {
        if (sc != null)
            sc.close();

        try {
            if(inputStream != null)
                inputStream.close();
        } catch(Exception e) {}
    }

}
