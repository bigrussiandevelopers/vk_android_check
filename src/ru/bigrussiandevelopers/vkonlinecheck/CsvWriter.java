package ru.bigrussiandevelopers.vkonlinecheck;

import java.io.IOException;
import java.io.PrintStream;

public class CsvWriter {

    private PrintStream writer = null;

    public CsvWriter(String filePath) throws IOException {
        writer = new PrintStream (filePath);
    }

    public void write(String id) {
        if(writer != null)
            writer.println(id);
    }

    public  void close() {
        writer.close();
    }
}
