package ru.bigrussiandevelopers.vkonlinecheck;


import java.io.IOException;
import java.net.*;
import java.io.*;
import java.util.Map;


public class VkPageDownloader {
    public static void main(String args[]){
        VkPageDownloader downloader = new VkPageDownloader("e1a2eb8d62d83ea43031c803592dee1535e7ac31d80246778a441");
        try {
            String testHTMLPage = downloader.getUrlAsString("https://m.vk.com/mail");
            System.out.println(testHTMLPage);
        } catch (MalformedURLException malformedUrlException){
            System.out.println("Wrong URL");
        } catch (IOException ioException){
            System.out.println("IO Exception");
        }
    }

    public VkPageDownloader(String remixsid){
        userAgentString = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) " +
                "AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25";
        userAgentString = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) " +
                "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36";
        String englishLanguageCookieValue = "3";
        cookieString = "remixsid=" + remixsid + "; remixlang=" + englishLanguageCookieValue + ";";
    }

    public String getUrlAsString(String stringURL) throws MalformedURLException, IOException{
        URL url = new URL(stringURL);
        URLConnection connection = url.openConnection();
        connection.addRequestProperty("Cookie", cookieString);
        connection.addRequestProperty("User-Agent", userAgentString);

        BufferedReader vkontakteBufferedReader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));

        String inputLine;
        String htmlString = "";

        while ((inputLine = vkontakteBufferedReader.readLine()) != null){
            htmlString += inputLine;
        }
        vkontakteBufferedReader.close();
        return htmlString;
    }

    public String getUrlAsString(String stringURL, Map<String,String> properties) throws MalformedURLException, IOException{
        URL url = new URL(stringURL);
        URLConnection connection = url.openConnection();

        connection.addRequestProperty("Cookie", cookieString);
        connection.addRequestProperty("User-Agent", userAgentString);

        for(Map.Entry property : properties.entrySet()){
            connection.addRequestProperty((String)property.getKey(), (String)property.getValue());
        }

        BufferedReader vkontakteBufferedReader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));

        String inputLine;
        String htmlString = "";

        while ((inputLine = vkontakteBufferedReader.readLine()) != null){
            htmlString += inputLine;
        }
        vkontakteBufferedReader.close();
        return htmlString;

    }

    private String userAgentString;
    private String cookieString;
}
