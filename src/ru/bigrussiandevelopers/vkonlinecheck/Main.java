package ru.bigrussiandevelopers.vkonlinecheck;


import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class Main {

    private static Logger logger;

    public static void main(String args[]) {

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

        String logFileName = dateFormat.format(date) + ".log";
        System.out.println(logFileName);
        logger = Logger.getLogger("VkAndroidParserLogger");
        FileHandler fh;
        try {
            fh = new FileHandler(logFileName);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            logger.info("Logger checking");
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CsvReader csvReader;
        try {
            csvReader = new CsvReader(args[2]);
        } catch (IOException e){
            logger.info("Something wrong with CSV file (IOException)\n");
            e.printStackTrace();
            return;
        }

        VkLogin login = new VkLogin(args[0], args[1]);

        String remixsid = null;
        try {
            remixsid = login.getSid();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        CsvWriter csvWriter = null;
        try {
            csvWriter = new CsvWriter(args[3]);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("Out file problems");
            return;
        }

        VkPageDownloader downloader = new VkPageDownloader(remixsid);
        VkWallParser parser = new VkWallParser(downloader, logger);
        String userId;

        HashSet<String> idSet = new HashSet<String>();

        while((userId = csvReader.getNextId()) != null){
            if (idSet.add(userId) && checkUserForAndroidPosts(parser, userId)) {
                csvWriter.write(userId);
            }
        }
        csvWriter.close();
        csvReader.close();
    }

    public static boolean checkUserForAndroidPosts(VkWallParser parser, String userId){
        logger.info("Checking user for using Android app: http://vk.com/id" + userId);
        boolean retry = true;
        boolean androidDetected = false;
        while (retry) {
            try {
                androidDetected = parser.checkUserForAndroidPosts(userId);
                retry = false;
            } catch (IOException e2) {
                logger.info("Internet failed:");
                e2.printStackTrace();
                logger.info("Trying again..");
            }
        }
        return androidDetected;
    }
}

