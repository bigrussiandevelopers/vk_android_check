package ru.bigrussiandevelopers.vkonlinecheck;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VkLogin {

    private static String USER_AGENT = "Mozilla/5.0";
    private static String sidPattern = "remixsid=([a-z0-9]{53})";
    private  String login = null;
    private String pass = null;

    public  VkLogin(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }

    private String sendPost() throws Exception {

        String url = "http://login.vk.com/?act=login";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add request header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "role=al_frame&pass=" + pass + "&email=" + login;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        return con.getHeaderFields().toString();
    }

    public String getSid() throws Exception {

            String data = sendPost();

            Pattern pattern = Pattern.compile(sidPattern);
            Matcher matcher = pattern.matcher(data);

            if (matcher.find()) {
                return matcher.group().substring(9); // 9: "remixsid=" shift
            }

            return null;
    }

}
